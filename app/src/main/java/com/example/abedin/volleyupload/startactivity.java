package com.example.abedin.volleyupload;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class startactivity extends Activity implements AdapterView.OnItemSelectedListener, View.OnClickListener, MediaScannerConnection.MediaScannerConnectionClient {

    private Spinner spinner;
    String[] items = new String[]{"Tshirt","Mug", "Keyring"};
    String selection ="";
    private Button buttonChoose;
    MediaScannerConnection conn;
    String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_start);
        buttonChoose = (Button) findViewById(R.id.buttonGo);
        spinner = (Spinner)findViewById(R.id.spinner_strt);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(startactivity.this,
                android.R.layout.simple_spinner_item,items);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        buttonChoose.setOnClickListener(this);
    }

    private void goNext(){

        if(selection.equalsIgnoreCase("T")) {
            copyAssets();
            Intent next = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(next);
        }
        else {
            Toast.makeText(this.getApplicationContext(), "Not yet",
            Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                selection="T";// Whatever you want to happen when the first item gets selected
                //Toast.makeText(this.getApplicationContext(), "Size S",
                        //Toast.LENGTH_LONG).show();
                // Whatever you want to happen when the second item gets selected
                break;
            case 1:
                selection="M";// Whatever you want to happen when the thrid item gets selected
                break;
            case 2:
                selection="K";// Whatever you want to happen when the thrid item gets selected
                break;


        }
    }

    private void copyAssets() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        for(String filename : files) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(filename);


                File nw_fil = new File("/sdcard/" + filename);
                //out = new FileOutputStream("/sdcard/" + filename);
                path = nw_fil.getPath();
                out = new FileOutputStream(nw_fil);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
                startScan();
            } catch(Exception e) {
                Log.e("tag", e.getMessage());
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

//    private void galleryAddPic(File f) {
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        //File f = new File(mCurrentPhotoPath);
//        Uri contentUri = Uri.fromFile(f);
//        mediaScanIntent.setData(contentUri);
//        this.sendBroadcast(mediaScanIntent);
//    }
private void startScan()
{
    if(conn!=null) conn.disconnect();
    conn = new MediaScannerConnection(startactivity.this,startactivity.this);
    conn.connect();
}


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if(v == buttonChoose){
            goNext();
        }
    }

    @Override
    public void onMediaScannerConnected() {
        try{
            conn.scanFile(path, "image/*");
        } catch (java.lang.IllegalStateException e){
        }
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        conn.disconnect();
    }
}
