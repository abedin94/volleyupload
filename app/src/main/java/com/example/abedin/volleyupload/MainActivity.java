package com.example.abedin.volleyupload;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Map;

import static android.app.PendingIntent.getActivity;
import static android.widget.AdapterView.INVALID_POSITION;

public class MainActivity extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Button buttonChoose;
    private Button buttonUpload;

    private ImageView imageView;


    private Spinner spinner_sz, spinner_clr;

    private Switch mySwitch;

    private Bitmap bitmap;

    private int PICK_IMAGE_REQUEST = 1;


    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";

    String[] items_sz = new String[]{"Size","S", "M", "L"};
    String[] items_clr = new String[]{"Colour", "Black","White","Ash"};
    
    

    String size="Free";

    String sleeve="";

    String color="B";

    File fil;

    FrameLayout custom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        custom= (FrameLayout) findViewById(R.id.container);
        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);


        imageView  = (ImageView) findViewById(R.id.imageView);

        spinner_sz = (Spinner)findViewById(R.id.spinnersz);
        ArrayAdapter<String>adapter_sz = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_item,items_sz);
        adapter_sz.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sz.setAdapter(adapter_sz);
        spinner_sz.setOnItemSelectedListener(this);


        spinner_clr = (Spinner)findViewById(R.id.spinnerclr);
        ArrayAdapter<String>adapter_clr = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_item,items_clr);
        adapter_clr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_clr.setAdapter(adapter_clr);
        spinner_clr.setOnItemSelectedListener(this);


        mySwitch = (Switch) findViewById(R.id.mySwitch);

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){

                    if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==1){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==2){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht) );
                    }
                   else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==3){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash) );
                    }
                   else if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                    }
                   else  if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==1){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                     }
                   else  if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==2){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht) );
                     }
                   else  if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==3){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash) );
                     }
                    else if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_m) );
                    }
                   else  if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==1){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_m) );
                     }
                   else  if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==2){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht_m) );
                     }
                   else  if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==3){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash_m) );
                     }
                    else if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_l) );
                    }
                   else  if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==1){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_l) );
                     }
                    else if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==2){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht_l) );
                     }
                   else  if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==3){
                         custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash_l) );
                     }
                    else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                    }


                    //custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                }

                else{
                    if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==1){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                    }
                   else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==2){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht) );
                    }
                   else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==3){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                    }
                   else if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==1){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                    }
                   else  if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==2){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht) );
                    }
                    else  if(spinner_sz.getSelectedItemPosition()==1 && spinner_clr.getSelectedItemPosition()==3){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_m) );
                    }
                    else  if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==1){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_m) );
                    }
                    else  if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==2){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht_m) );
                    }
                    else  if(spinner_sz.getSelectedItemPosition()==2 && spinner_clr.getSelectedItemPosition()==3){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash_m) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_l) );
                    }
                    else  if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==1){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_l) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==2){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht_l) );
                    }
                    else  if(spinner_sz.getSelectedItemPosition()==3 && spinner_clr.getSelectedItemPosition()==3){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash_l) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==2){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                    }
                    else if(spinner_sz.getSelectedItemPosition()==0 && spinner_clr.getSelectedItemPosition()==0){
                        custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                    }


                }

            }
        });

        //check the current state before we display the screen

        buttonChoose.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);



    }



    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(bmp!=null) {
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            return encodedImage;
        }
        else{
            return "";
        }
    }

    private void uploadImage(){
        //Showing the progress dialog
        if(mySwitch.isChecked()){
          sleeve="Full";
        }
        else {
            sleeve="Half";

        }
                //Converting Bitmap to String
               // String image = getStringImage(bitmap);
        if(bitmap!=null) {
            Intent i = new Intent(this, ResultActivity.class);
            i.putExtra("tslv", sleeve);
            i.putExtra("tsize", size);
            i.putExtra("tcolor", color);
            //i.putExtra("timage", image);
            startActivity(i);
        }
        else {
            Toast.makeText(this.getApplicationContext(), "Upload desiresd image to continue",
                    Toast.LENGTH_LONG).show();
        }

    }


    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);

                FileOutputStream out = null;
                fil = new File(getFilesDir(), "design.png");
                //FileOutputStream out = null;
                try {
                    out = new FileOutputStream(fil);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {

        if(v == buttonChoose){
            showFileChooser();
        }

        if(v == buttonUpload){
            uploadImage();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        if(spinner.getId() == R.id.spinnersz)
        {
            switch (position) {
                case 0:
                    size="Free";

                    if(mySwitch.isChecked()){
                        //Fullslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash) );
                        }

                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }

                    }

                    else {
                        //Hlfslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }

                    }
                    break;

                case 1:

                    size="S";// Whatever you want to happen when the first item gets selected
                    // Whatever you want to happen when the second item gets selected

                    if(mySwitch.isChecked()){
       //Fullslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }

                    }

                    else {
       //Hlfslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }

                    }
                    break;





                case 2:
                    size="M";

                    if(mySwitch.isChecked()){
                        //Fullslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_m) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht_m) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash_m) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_m) );
                        }


                    }

                    else {
                        //Hlfslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_m) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht_m) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash_m) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_m) );
                        }

                    }

                    break;

                case 3:
                    size="L";

                    if(mySwitch.isChecked()){
                        //Fullslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_l) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht_l) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_l) );
                        }

                    }

                    else {
                        //Hlfslv
                        if(spinner_clr.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_l) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_wht_l) );
                        }
                        else if(spinner_clr.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_ash_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_l) );
                        }

                    }

                    break;

            }
        }

        else if(spinner.getId() == R.id.spinnerclr)
        {
            switch (position) {
                case 0:

                    color="B";// Whatever you want to happen when the thrid item gets selected

                    if(mySwitch.isChecked()){
                    //FullSlv
                        if(spinner_sz.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_m) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }
                    }
                    else {
                     //halfSlv
                        if(spinner_sz.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_m) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }

                    }

                    break;

                case 1:

                    color="B";// Whatever you want to happen when the first item gets selected
                    // Whatever you want to happen when the second item gets selected

                    if(mySwitch.isChecked()){
                        //FullSlv
                        if(spinner_sz.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_m) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt) );
                        }
                    }
                    else {
                        //halfSlv
                        if(spinner_sz.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_m) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.tshirt) );
                        }

                    }

                    break;

                case 2:

                    color="W";// Whatever you want to happen when the thrid item gets selected

                    if(mySwitch.isChecked()){
                        //FullSlv
                        if(spinner_sz.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht_m) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_wht) );
                        }
                    }
                    else {
                        //halfSlv
                        if (spinner_sz.getSelectedItemPosition() == 1) {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_wht));
                        } else if (spinner_sz.getSelectedItemPosition() == 2) {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_wht_m));
                        } else if (spinner_sz.getSelectedItemPosition() == 3) {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_wht_l));
                        }
                        else {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_wht));
                        }
                    }

                    break;

                case 3:

                    color="A";// Whatever you want to happen when the thrid item gets selected

                    if(mySwitch.isChecked()){
                        //FullSlv
                        if(spinner_sz.getSelectedItemPosition()==1) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==2) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash_m) );
                        }
                        else if(spinner_sz.getSelectedItemPosition()==3) {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash_l) );
                        }
                        else {
                            custom.setBackgroundDrawable( getResources().getDrawable(R.drawable.full_tshirt_ash) );
                        }
                    }
                    else {
                        //halfSlv
                        if (spinner_sz.getSelectedItemPosition() == 1) {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_ash));
                        } else if (spinner_sz.getSelectedItemPosition() == 2) {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_ash_m));
                        } else if (spinner_sz.getSelectedItemPosition() == 3) {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_ash_l));
                        }
                        else {
                            custom.setBackgroundDrawable(getResources().getDrawable(R.drawable.tshirt_ash));
                        }
                    }

                    break;

            }
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //size="Free";
    }
}
