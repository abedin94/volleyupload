package com.example.abedin.volleyupload;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ResultActivity extends Activity implements View.OnClickListener {

    private EditText editTextName,editTextEmail,editTextPhone,editTextQnty;
    private  String Image,slv,Size,color;
    private Button buttonsubmit;
    Bitmap bitmap;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        editTextName = (EditText) findViewById(R.id.editTextname);
        editTextEmail = (EditText) findViewById(R.id.editTextemail);
        editTextPhone = (EditText) findViewById(R.id.editTextphone);
        editTextQnty = (EditText) findViewById(R.id.editTextqnty);
        txt=(TextView)findViewById(R.id.textView3);
        buttonsubmit= (Button) findViewById(R.id.buttonSubmit);
        buttonsubmit.setOnClickListener(this);

        Intent intent = getIntent();
        slv = intent.getExtras().getString("tslv");
        Size = intent.getExtras().getString("tsize");
        color = intent.getExtras().getString("tcolor");
        //Image = intent.getExtras().getString("timage");

        editTextQnty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!editTextQnty.getText().toString().equalsIgnoreCase("")){
                    int x= Integer.parseInt(editTextQnty.getText().toString());
                    int y = (150*x)+50;
                    txt.setText(Integer.toString(y));
                }
            }
        });

    }

    private void do_the_job(){
        if(editTextName.getText().toString().trim().length()!=0 && editTextEmail.getText().toString().trim().length()!=0 && editTextPhone.getText().toString().trim().length()!=0 && editTextQnty.getText().toString().trim().length()!=0) {
//            Log.w("myApp", Image);
//            try {
//                byte[] encodeByte = Base64.decode(Image, Base64.DEFAULT);
//                bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
//
//            } catch (Exception e) {
//                e.getMessage();
//            }

            //Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            // Store image in Devise database to send image to mail

//you can create a new file name "test.jpg" in sdcard folder.
            //FileOutputStream out = null;
            File fil = new File(getFilesDir(), "design.png");
//            //FileOutputStream out = null;
//            try {
//                out = new FileOutputStream(fil);
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
//                // PNG is a lossless format, the compression factor (100) is ignored
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    if (out != null) {
//                        out.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }

            String formatted = "Customer name : " + editTextName.getText().toString().trim() + "\n" + "Email : " + editTextEmail.getText().toString().trim() + "\n" + "Phone : " +
                    editTextPhone.getText().toString().trim() + "\n" + "Quantity : " + editTextQnty.getText().toString().trim() + "\n" +
                    "Sleeve : " + slv + "\n" + "Size : " + Size+"\n"+"Colour : "+ color;
//
////        String path = MediaStore.Images.Media.insertImage(getContentResolver(), decodedByte,"title", null);
////        Uri screenshotUri = Uri.parse(path);
//        Intent emailIntent1 = new Intent(android.content.Intent.ACTION_SEND);
//        emailIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        emailIntent1.putExtra(android.content.Intent.EXTRA_EMAIL, "abedin94@gmail.com");
//        emailIntent1.putExtra(android.content.Intent.EXTRA_SUBJECT, "Order");
//        emailIntent1.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(fil));
//        emailIntent1.setType("image/png");
//        startActivity(Intent.createChooser(emailIntent1, "Send email using"));
            //sales@ghuri.online
            SendMail sm = new SendMail(this, "hazzaz77@gmail.com", "Chapakhana order", formatted, fil.getPath());
            sm.execute();
        }
        else {
            Toast.makeText(this.getApplicationContext(), "Please give valid Info in all the fields",
                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View v) {
        if(v == buttonsubmit){
            do_the_job();
        }
    }
}
